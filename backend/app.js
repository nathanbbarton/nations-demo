const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const routes = require("./routes/routes.js");
const models = require("./models");
// SEED DATA
const nations = require("./nationsData.json");

const app = express();

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

routes(app);

models.connectDb().then(async () => {
    console.log(models)
    try {
        await Promise.all(nations.map((nation) => models.Nation.create(nation)));
        console.log("Data seeded correctly");
        const server = app.listen(8080, () => console.log("app running on port.", server.address().port));
    } catch (err) {
        console.error(err)
    }
});



