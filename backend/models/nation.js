
/**
 * Here we setup a basic model of what a nation looks like to mongodb
 */

const mongoose = require("mongoose");

const nationSchema = new mongoose.Schema({
    date: {
        type: Date,
        unique: false
    },
    name: {
        type: String,
        unique: false
    },
    number: {
        type: Number,
        unique: false
    },
});

module.exports = mongoose.model("Nation", nationSchema);
