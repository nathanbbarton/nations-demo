const mongoose = require("mongoose");
const Nation = require("./nation");

const connectDb = () => mongoose.connect(process.env.DATABASE_URL);

module.exports = {
    Nation: Nation,
    connectDb: connectDb
};