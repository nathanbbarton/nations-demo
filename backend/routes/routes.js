/**
 * A place to put basic routes to serve the api
 * in this we setup a basic API endpoint to return all united nation memebers.
 */
const models = require("../models");

const appRouter = (app) => {
  app.get("/", (req, res) => {
    res.status(200).send("Welcome to our restful API");
  });

  //RETURN ALL MEMBER NATIONS for frontend to display
  app.get("/allMemberNations", async (req, res) => {
    
    try {
      const nations = await models.Nation.find({})//call to mongodb to get all the nations

      console.log(nations);

      return res.status(200).send({ nations: nations }); //return the nations
    } catch (err) {
      console.error(err);
    }

  });
}

module.exports = appRouter;
