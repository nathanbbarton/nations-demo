var fs = require('fs'),
    readline = require('readline');

var rd = readline.createInterface({
    input: fs.createReadStream('./nationsData.txt'),
    output: process.stdout,
    console: false,
    terminal: false
});

const nations = [];

const parse = async () => {
    return new Promise ((resolve, reject) => {
        rd.on('line', function(line) {
            const lineArray = line.split(' ');
        
            //deconstruct the array
            const date = lineArray.pop();
            const name = lineArray.shift();
        
            const nation = {
                name: lineArray.join(' '),
                number: name,
                date: new Date(date)
            }
        
            nations.push(nation);
        });

        rd.on('close', () => {
            resolve(nations);
        });
    });
}

parse()
    .then(nations => {
        console.log(nations)

        fs.writeFile('nationsData.json', JSON.stringify(nations, null, 2), (err) => {
            if (err) throw err;
        });
    });


