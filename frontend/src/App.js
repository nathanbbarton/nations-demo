import React, { Component } from 'react';
import { Container } from "@material-ui/core";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lastAccession: {},
            numParties: 0,
            numNonParties: 0
        };
    }

    // ON RUN TIME CALL OUT TO BACKEND TO GET ALL THE UNITED NATIONS DATA
    componentDidMount() {
        fetch(`http://localhost:8080/allMemberNations`)
            .then(res => res.json())
            .then(nationsObj => {

                const nations = nationsObj.nations;

                const sortedByDate = nations
                .sort((nation1, nation2) =>new Date(nation1.date) - new Date(nation2.date))

                console.log(sortedByDate);

                this.setState({
                    nations: nations,
                    numParties: nations.filter(nation => nation.number > 0).length,
                    numNonParties: nations.filter(nation => nation.number <= 0).length,
                    lastAccession: sortedByDate.pop()
                });
            })
            .catch(err => console.error(err));
    }


    render() {

        const { nations, numParties, numNonParties, lastAccession } = this.state;

        if (!nations) return null; //api has not responded yet and we have no nations to display

        return (
            <Container maxWidth="md">
                    <h1> Parties of the Convention on Biological Diversitry</h1>

                    <h2>Number of Parties: {numParties}</h2>
                    <h2>Last Accesssion: {lastAccession.name + " on " + lastAccession.date}</h2>

                    <table style={{ "border": "1px solid #aaaaaa"}}>
                        <tr>
                            <th>#</th>
                            <th>Countries</th>
                            <th>Accession Date</th>
                        </tr>

                        {nations.filter(nation => nation.number > 0) //get rid of nonparties
                                .sort((nation1, nation2) => nation1.number - nation2.number) //sort by nation number
                                .map(nation => 
                                    <tr>
                                        <td>{nation.number}</td>
                                        <td>{nation.name}</td>
                                        <td>{nation.date}</td>
                                    </tr>)}
                    </table>

                    <h2>Number of Non-Parties: {numNonParties} </h2>

                    <table style={{ "border": "1px solid #aaaaaa"}}>
                        <tr>
                            <th>#</th>
                            <th>Countries</th>
                        </tr>

                        {nations.filter(nation => nation.number <= 0) //get rid of nonparties
                                .map((nation, index) => 
                                    <tr>
                                        <td>{index + 1}</td>
                                        <td>{nation.name}</td>
                                    </tr>)}
                    </table>

            </Container>
        );
    }
}

export default App;
